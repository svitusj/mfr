package usj.svit.approach.flimea.core.operator.mutation;

import usj.svit.approach.flimea.core.config.EAConfigTextEMF;
import usj.svit.approach.flimea.core.population.IPopulation;
import usj.svit.architecture.individual.IIndividualTextEMF;
import usj.svit.architecture.individual.impl.Chromosome;
import usj.svit.architecture.individual.impl.IndividualTextEMF;
import usj.svit.architecture.individual.impl.eof.EObjectFragmentable;
import usj.svit.architecture.utils.MathUtils;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class ReformulationReplacement extends Reformulation implements IMutation<IIndividualTextEMF> {
		
	private ArrayList<String> queryKeywords;
	
	ArrayList<String> includedTerms=new ArrayList<String>();

	
	
	@Override
	public void mutate(EAConfigTextEMF context) {
		
		IPopulation<IIndividualTextEMF> population=context.getPopulation();
		
		
		
		initializeRelevantDocsAndCorpus(population.getIndividuals());
		
		
		//copying the offspring to do not modify the selected parents
		List<IIndividualTextEMF> offspring=context.getOffSpring();
		
		List<IIndividualTextEMF> mutatedOffspring = new ArrayList<IIndividualTextEMF>();
		Chromosome parent1 = offspring.get(0).getCopyOfGenes();
		Chromosome parent2 = offspring.get(1).getCopyOfGenes();
		IndividualTextEMF indiv1=new IndividualTextEMF(parent1,offspring.get(0).getParent());
		indiv1.setText(offspring.get(0).getText());
		
		IndividualTextEMF indiv2=new IndividualTextEMF(parent2,offspring.get(1).getParent());
		indiv2.setText(offspring.get(1).getText());
		
		mutatedOffspring.add(indiv1);
		mutatedOffspring.add(indiv2);
		
		context.getOffSpring().clear();
			
		for(IIndividualTextEMF individual : mutatedOffspring) {
			
			queryKeywords= getFilteredText(individual,individual.getParent().getSize());
			
			ArrayList<String> termsToReplace=runReplacement(relevantDocs, queryKeywords, queryKeywords.size());
			
			
			
			EObjectFragmentable parent=individual.getParent();
			//String text;
			String [] text;
			int parentSize=parent.getSize();
			int pos=0;
			
			int flippedToPositive=0;
			int flippedToNegative=0;
			int termsToBeReplaced=termsToReplace.size();
			while(pos<parentSize && !termsToReplace.isEmpty())
			{
				
					text=parent.getFilteredTextForPosition(pos);
					for(int t=0;t<text.length;t++)
					{
						boolean genContainsTerms=termsToReplace.contains(text[t]);
						boolean positiveGen=individual.getGen(pos);
						if(genContainsTerms && !positiveGen) 
						{
						   individual.flipGen(pos);
						   System.out.println("Flipped gen "+pos+" to 1 due to the replaced term: "+text[t]);
						   termsToReplace.remove(text[t]);
						   countAffected++;
						   flippedToPositive++;
						   break;
						}
						else if (!genContainsTerms && positiveGen && flippedToNegative<termsToBeReplaced)
						{
							individual.flipGen(pos);
							System.out.println("-- Flipped gen "+pos+" to 0 due to the replaced term: "+text[t]);
							countAffected++;
							flippedToNegative++;
						}
						
					}
				
				pos++;
			}	
		
			System.out.println("\n ** Gens flipped to 1: "+flippedToPositive+", flipped to 0: "+flippedToNegative);
		}
		countCalls++;
		
		//the offspring is updated with the mutated individuals
		context.getOffSpring().addAll(mutatedOffspring);
	}
	/**
	 * randomly mutates a bit of the individual
	 */
	@Override
	public void forceMutation(IIndividualTextEMF individual) {
		int index = MathUtils.getRandomInt(individual.getParentModelSize());
		individual.flipGen(index);
		countCalls++;
		countAffected++;
	}

	
	
	/**
	 * same probability for each model element of being added/removed
	 * 
	 * @param BinaryIndividual
	 * @return Individual
	 */
	
	private ArrayList<String> runReplacement(ArrayList<HashMap<String,Integer>> corpusDocs, ArrayList<String> queryKeywords, int numTermsToReplace)
	{
		
		
		ArrayList<HashMap<String,Double>> resultDSInCorpus=getDSValues(corpusDocs);
	
		Map<String, Double> resultDSMean=getDSMeanValues(resultDSInCorpus);
		Map<String, Double> sortedTermWeights=sortByComparator(resultDSMean, false);
		

		ArrayList<String> result=new ArrayList<String>();
		for (Entry<String, Double> entry : sortedTermWeights.entrySet())
		{
			if(numTermsToReplace>0)
			{
			String word=entry.getKey();
			
				numTermsToReplace--;
				result.add(word);
			
			}
			else break;
		}

		return result;
	}
	
	private int getNumOccurrencesInDocuments(ArrayList<HashMap<String,Integer>> documents, String word)
	{
		int result=0;
		//number of times that a term appears in a document
		for (HashMap<String,Integer> doc: documents)
		{
			if(doc.containsKey(word)) result=result+doc.get(word);
		}
	
		return result;
	}
	private ArrayList<HashMap<String,Double>> getDSValues(ArrayList<HashMap<String,Integer>> corpusDocs)
	{
		ArrayList<HashMap<String,Double>> resultDSValuesInDocs=new ArrayList<HashMap<String,Double>>();
		for (HashMap<String,Integer> doc: corpusDocs)
		{
			HashMap<String,Double> resultDoc= new HashMap<String,Double> ();
			
			for (HashMap.Entry<String, Integer> entry:doc.entrySet())
			{
				String word = entry.getKey();
			    int frequencyInDoc = entry.getValue();
			    //int numCorpusDocsHasTheWord=getNumDocsHasAWord(corpusDocs, word);
			    int freqInCorpus=getNumOccurrencesInDocuments(corpusDocs, word);
			    double value=Math.log((double)frequencyInDoc/(double)freqInCorpus);
			
			    
			    resultDoc.put(word, value);
			}
			resultDSValuesInDocs.add(resultDoc);
		
		}
		return resultDSValuesInDocs;
	}
	private HashMap<String, Double> getDSMeanValues(ArrayList<HashMap<String,Double>> resultDSValuesInDocs)
	{
		HashMap<String,Double> resultDSMeanValues= new HashMap<String,Double> ();
		//for each term in copus docs, its DS value is added to calculate the mean
		for (HashMap<String,Double> doc: resultDSValuesInDocs)
		{
			
			
			for (HashMap.Entry<String, Double> entry:doc.entrySet())
			{
				String word = entry.getKey();
			    double dsValue= entry.getValue();
		
			    if(resultDSMeanValues.containsKey(word)) 
			    	{
			    		double newDsValue=resultDSMeanValues.get(word)+dsValue;
			    		resultDSMeanValues.replace(word, newDsValue);
			    		
			    	}
			    else resultDSMeanValues.put(word,dsValue);
			    
			}
		}
			
			//now for each term the mean is obtained
			for (HashMap.Entry<String, Double> entry:resultDSMeanValues.entrySet())
			{
				String word = entry.getKey();
			    double dsValue= entry.getValue();
				resultDSMeanValues.replace(word, dsValue/corpusDocs.size());
			}
			
		
		
		return resultDSMeanValues;
	}
	
}