package usj.svit.approach.flimea.core.operator.mutation;

import usj.svit.approach.flimea.core.config.EAConfigTextEMF;
import usj.svit.approach.flimea.core.operator.BaseOperation;
import usj.svit.architecture.individual.IIndividualTextEMF;
import usj.svit.architecture.utils.MathUtils;

public class MutationRandomGen extends BaseOperation implements IMutation<IIndividualTextEMF> {

	/**
	 * same probability for each model element of being added/removed
	 * 
	 * @param BinaryIndividual
	 * @return Individual
	 */
	private void mutate(IIndividualTextEMF individual,EAConfigTextEMF context) {
		if(Math.random() <= context.MUTATION_PROBABILITY)
			forceMutation(individual);
		
		
		
	}

	@Override
	public void mutate(EAConfigTextEMF context) {
		for(IIndividualTextEMF individual : context.getOffSpring()) {
			mutate(individual,context);
			countCalls--;
		}
		countCalls++;
	}

	@Override
	public void forceMutation(IIndividualTextEMF individual) {
		int index = MathUtils.getRandomInt(individual.getParentModelSize());
		individual.flipGen(index);
		countAffected++;
		countCalls++;
	}
		
	
}
