package usj.svit.experiment.tse19.approaches;

import usj.svit.approach.flimea.core.config.EAConfigTextEMF;
import usj.svit.approach.flimea.core.ea.BaseEATextEMF;
import usj.svit.approach.flimea.core.pre.IPreProcessor;
import usj.svit.architecture.individual.IIndividualEMF;
import usj.svit.architecture.individual.IIndividualTextEMF;
import usj.svit.architecture.oracle.TestCaseTextEMF;
import usj.svit.architecture.results.EMFResultSingle;
import usj.svit.architecture.results.IEMFResult;

public class EA_Baseline extends BaseEATextEMF {

	static int id_counter=0;
	int id;
	
	public EA_Baseline(EAConfigTextEMF config) {
		super(config);
		id = id_counter;
		id_counter++;
	}

	@Override
	public IEMFResult<IIndividualEMF> run(TestCaseTextEMF testCase) {
		
		this.config.setCurrentTestCase(testCase);
		
		this.config.getPopulation().setPopulation(config.getPopulator().generateInitialPopulation(config));
		
		for (IPreProcessor<IIndividualTextEMF> preprocessor : config.getPreprocessors()) {
			preprocessor.process(config);
		}
		
		for (IIndividualTextEMF element : config.getPopulation().getIndividuals()) {
			element.updateTextsAfterRecombinations();
		}
		
		this.config.getFitness().setBoundaries(this.config);
		
		this.loop();
		return new EMFResultSingle<IIndividualEMF>(config.getPopulation().get(0));
		
	}
	
	@Override
	public void loop() {

		long t0 = System.currentTimeMillis();
		
		while (true) {
			
			config.getFitness().assess(config);
			
			config.getPopulation().sortPopulationDescendant();
			
			System.out.println("Gen: "+config.getCurrentGeneration() +" - "+config.getPopulation().get(0).getFitness()  +" - "+config.getPopulation().get(config.MAX_POPULATION-1).getFitness() +  " - "+(System.currentTimeMillis()-t0));
			t0 = System.currentTimeMillis();
			
			if (config.stopConditionMeet()) {
				//System.out.println("Gen: "+config.getCurrentGeneration() +" - "+config.getPopulation().get(0).getFitness());
				return;
			}

			config.getSelection().selectParents(config);
			config.getCrossover().crossover(config);
			config.getMutation().mutate(config);

			for (IIndividualTextEMF element : config.getOffSpring()) {
				element.updateTextsAfterRecombinations();
			}
			
			config.getReplace().replace(config);
			
			config.increaseCurrentGeneration();
		}
	}

	@Override
	public String getID() {
		return "EA_"+id;
	}

}

