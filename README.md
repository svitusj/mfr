# README - MODEL FRAGMENT REFORMULATION VARIANTS AS GENETIC OPERATIONS#

Repository containing: 
1) the results of locating features in software models using an Evolutionary Algorithm with model fragment reformulations as genetic operations, and 2) the source code of the following operations: 

* Baseline 1: single point crossover + random mutation.
* Baseline 2: mask crossover + random mutation.
* Variant 1: Model Fragment Reformulation (Expansion).
* Variant 2: Model Fragment Reformulation (Replacement).
* Variant 3: Model Fragment Reformulation (Reduction).
* Variant 4: Model Fragment Reformulation (Selection).